**Pinoylinux Group**
https://www.facebook.com/groups/pinoylinux

Author: Mike Cabalin


[[_TOC_]]

## Linux File Server Install

Samba enables Linux / Unix machines to communicate with Windows machines in a network. Samba is open source software. Originally, Samba was developed in 1991 for fast and secure file and print share for all clients using the SMB protocol. Since then it has evolved and added more capabilities

Requirements

| Category | Requirements, Conventions or Software Version Used | 
| ---      | ---      | 
| System   | Installed or upgraded Ubuntu 20.04 Focal Fossa  | 
| Software  | Samba  | 
| Other   | Privileged access to your Linux system as root or via the sudo command | 

Ubuntu Server 20 Installation link guide
https://www.makeuseof.com/ubuntu-server-guide/


## How to configure Samba Server share on Ubuntu 20.04

<pre>
$ sudo apt install tasksel
$ sudo tasksel install samba-server
</pre

We will be starting with a fresh clean configuration file, while we also keep the default config file as a backup for reference purposes. Execute the following linux commands to make a copy of an existing configuration file and create a new /etc/samba/smb.conf configuration file>

<pre>
$ sudo cp /etc/samba/smb.conf /etc/samba/smb.conf_backup
$ sudo bash -c 'grep -v -E "^#|^;" /etc/samba/smb.conf_backup | grep . > /etc/samba/smb.conf'
</pre>

Samba has its own user management system. However, any user existing on the samba user list must also exist within /etc/passwd file. If your system user does not exist yet, hence cannot be located within /etc/passwd file, first create a new user using the useradd command before creating any new Samba user.Once your new system user eg. linuxconfig exits, use the smbpasswd command to create a new Samba user

<pre>
$useradd aeonmike
$passwd aeonmike
$ sudo smbpasswd -a aeonmike
New SMB password:
Retype new SMB password:
Added user linuxconfig.
</pre>

Next step is to add the home directory share

## Samba Scripts

Run the samba script to download necessary packages and configurations


## Install LVM

What is LVM?

Logical Volume Manager allows for a layer of abstraction between your operating system and the disks/partitions it uses.

LVM or Logical Volume Manager makes management of disk space on a Linux machine easier to manage.  Features of LVM include:

- Create, resize, move and delete partitions on the disks
- Manage the space of multiple hard disks by creating logical volumes
- Operate on the system without knowing the total space of any partition
- Space of any partition can be resized or assigned to any low space partition at any time
- Change the file system of any existing partition or remove any partition from the system quickly
- Create snapshots of any running system
- Creates striped logical volumes of the disks
- Creation of software RAID partitions or standard partitions of a single disk
- Volumes of the disk can be extended on the fly to provide more flexibility to the system based on requirements.

Three main components of LVM are physical volumes, logical volumes and volume groups. The block devices that are used to store logical volumes are called physical volumes. Each logical volume holds a file system which corresponds to a partition and a unique name is assigned for each logical volume. The collection of all physical and logical volumes is called volume group.

In this tutorial, you will learn different utilities of LVM, such as how to create or remove a new volume, how to find out the information about current volumes, how to resize existing volumes of the disk on Ubuntu and creating snapshot of a running system.

**Creating, Resizing and Removing LVM Components**

You must have root privilege to run any LVM command. So, first of all, you have to run LVM with root password.

<pre>
$ sudo lvm
</pre>

Run the command to find all existing block devices. In the output, it is shown that there are two partitions which are /dev/sda1 and /dev/sda5, 1 disk in /dev/sdb and no physical volumes.

<pre>
lvm> lvmdiskscan
</pre>

**Create Physical Volume**

pvcreate command is used to create physical volume from storage devices. Here, /dev/sdb is used for creating a physical volume. Run the commands to create a physical volume of 5GB. The success message will appear if the physical volume creates successfully.

<pre>pvcreate /dev/sdb /dev/sdc /dev/sdd</pre>

You can also use pvdisplay command to find the existing physical volumes.

<pre> pvdisplay </pre>

**Create Volume Group**

You can create a volume group after creating a physical volume. vgcreate command is used to create new volume group.

<pre>
vgcreate Pinoylinux_VG /dev/sdb /dev/sdc /dev/sdd
</pre>

**Create Logical Volume**

You can create logical volume of specific size or all remaining free space. lvcreate command is used to create logical volume.

<pre>
lvcreate -L 10G -n LV1 Pinoylinux_VG
</pre>

or

<pre>
lvcreate -l 100%FREE -n LV1 Pinoylinux_VG
</pre>

## Create XFS File System to LV1

<pre>
mkfs.xfs -L /fileshare /dev/mapper/PinoyLinux_VG/LV1 
</pre>

To get the UUID, run the command below

<pre>
lsblk -fs -d /dev/mapper/PinoyLinux_VG/LV1
</pre>

## Add Mount to FSTAB

<pre>
UUID={UUID}      /fileshare           xfs     defaults    0 0
</pre>


## How to Take ‘Snapshot of Logical Volume and Restore’ in LVM

LVM Snapshots are space efficient pointing time copies of lvm volumes. It works only with lvm and consume the space only when changes are made to the source logical volume to snapshot volume. If source volume has a huge changes made to sum of 1GB the same changes will be made to the snapshot volume. Its best to always have a small size of changes for space efficient. Incase the snapshot runs out of storage, we can use lvextend to grow. And if we need to shrink the snapshot we can use lvreduce.

**Creating LVM Snapshot**

First, check for free space in volume group to create a new snapshot using following ‘vgs‘ command.

<pre>
vgs
lvs
</pre>


<pre>lvcreate -L 1GB -s -n thanos_snap /dev/PinoyLinux_VG/LV1</pre>

The above commands does the same thing:
<pre>
-s – Creates Snapshot
-n – Name for snapshot
</pre>

Now, list the newly created snapshot using following command.

<pre>
lvs
</pre>

Let’s add some new files into Pinoylinux LV1. 

<pre>
lvs
</pre>

## Extend Snapshot in LVM
If we need to extend the snapshot size before overflow we can do it using.

<pre>lvextend -L +1G /dev/Pinoylinux_VG/LV1</pre>

## Restoring Snapshot or Merging

To restore the snapshot, we need to un-mount the file system first.

<pre>unmount -l /fileshare</pre>

Here our mount has been unmounted, so we can continue to restore the snapshot. To restore the snap using command lvconvert.

<pre>lvconvert --merge /dev/mapper/PinoyLinux_VG/thanos_snap</pre>

After the merge is completed, snapshot volume will be removed automatically. Now we can see the space of our partition using df command.

df -Th

After the snapshot volume removed automatically. You can see the size of logical volume.

lvs




## Extend the Snapshots automatically

We can do it using some modification in conf file. For manual we can extend using lvextend.

Open the lvm configuration file using your choice of editor.

nano /etc/lvm/lvm.conf


Search for word autoextend. By Default the value will be similar to below.

<pre>
 grep -E  ‘^\s*snapshot_auto’ /etc/lvm/lvm.conf 
 snapshot_autoextend_threshold = 100
 snapshot_autoextend_percent = 20
</pre>

Change the 100 to 75 here, if so auto extend threshold is 75 and auto extend percent is 20, it will expand the size more by 20 Percent

If the snapshot volume reach 75% it will automatically expand the size of snap volume by 20% more. Thus,we can expand automatically. 

Save and exit 






